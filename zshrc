alias :q='exit'
alias z='zellij'

# enable my bins
export PATH="$HOME/bin:$PATH"

# zsh vi mode
bindkey -v

# nvim
export EDITOR='nvim'
export VISUAL='nvim'

# Elixir Stuff
export ERL_AFLAGS="-kernel shell_history enabled"
# Elixir Stuff End

# postgresapp
export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/12/bin

# direnv
eval "$(direnv hook zsh)"

##### FZF #####
export FZF_DEFAULT_COMMAND="rg --files --hidden --follow --glob '!.git'"
# [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# shell integration allows ^r to use fzf for reverse search
source <(fzf --zsh)
bindkey -M viins '^p' fzf-history-widget
bindkey -M vicmd '^p' fzf-history-widget
##### End FZF #####

export PATH="/usr/local/sbin:$PATH"

export PATH="/usr/local/opt/node@16/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/node@16/lib"
export CPPFLAGS="-I/usr/local/opt/node@16/include"

# allow local ./bin
export PATH="./bin:$PATH"

# asdf
. "$HOME/.asdf/asdf.sh"
# append completions to fpath
fpath=(${ASDF_DIR}/completions $fpath)

# put lazygit config (and others?) in the right place
export XDG_CONFIG_HOME="$HOME/.config"

# turned off because of the chance of errors when it picks the wrong directory
# eval "$(zoxide init --cmd cd zsh)"

eval "$(starship init zsh)"

####### ZSH PLUGINS #######
source /usr/local/opt/zinit/zinit.zsh # installed via brew install zinit

zi light z-shell/zsh-lsd

# syntax highlighting
zinit light zsh-users/zsh-syntax-highlighting

# zsh completions
zinit light zsh-users/zsh-completions
autoload -Uz compinit && compinit

# zsh auto suggestions
zinit light zsh-users/zsh-autosuggestions
bindkey '^f' autosuggest-accept # accept
# bindkey '^ ' autosuggest-execute # accept and execute
# bindkey '^ ' autosuggest-clear # clear
# bindkey '^ ' autosuggest-fetch # Fetches a suggestion (works even when suggestions are disabled).
# bindkey '^ ' autosuggest-disable
# bindkey '^ ' autosuggest-enable
# bindkey '^ ' autosuggest-toggle # toggle between disable and enable
bindkey -M vicmd 'k' history-search-backward
bindkey -M vicmd 'j' history-search-forward
# bindkey '^p' history-search-backward
# bindkey '^n' history-search-forward
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups
# zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
# zstyle ':completion:*' list-colors '${(s.:.)LS_COLORS}' # Maybe only works correctly on linux?

# Change cursor shape for different vi modes.
function zle-keymap-select () {
  case $KEYMAP in
    vicmd) echo -ne '\e[1 q';; # block
    viins|main) echo -ne '\e[5 q';; # beam
  esac
}
zle -N zle-keymap-select
zle-line-init() {
  zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
  echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
# End Change cursor shape for different vi modes.

# Yazi
function yy() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

# zsh syntax highlighting (must be at end of file)
zinit light zsh-users/zsh-syntax-highlighting
